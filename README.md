# workshop-poster

A script to generate a poster for our workshop: 'Zine-machines' @ Sign Gallery, Groningen. 19 & 20/10/2018

Have a look in [poster.py] for a description of the process.

[poster.py]: https://gitlab.constantvzw.org/zine-machines/workshop-poster/blob/master/poster.py

Have a look at [generated/poster.pdf] for the current poster output.

[generated/poster.pdf]: https://gitlab.constantvzw.org/zine-machines/workshop-poster/blob/master/generated/poster.pdf

## Hack On It

Get a local copy with:

```bash
$ git clone https://gitlab.constantvzw.org/zine-machines/workshop-poster
$ cd workshop-poster
```

Install [pipenv] and then run:

[pipenv]: https://pipenv.readthedocs.io/en/latest/install/

```bash
$ pipenv install --dev --three
```

And then generate the poster with:

```bash
$ pipenv run python poster.py
```

You can then open the `generated/poster.pdf` file.

## The Data Set

We got our `datasets/titles.txt` from [this Kaggle Dataset].

[this Kaggle Dataset]: https://www.kaggle.com/residentmario/wikipedia-article-titles#titles.zip
