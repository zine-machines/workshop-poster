install:
	@pipenv install --dev --three
.PHONY: install

pdf:
	@pipenv run python poster.py
.PHONY: pdf

open:
	@evince generated/poster.pdf
.PHONY: open
